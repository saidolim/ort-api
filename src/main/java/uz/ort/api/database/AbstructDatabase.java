package uz.ort.api.database;

import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.jdbc.JDBCClient;
import io.vertx.ext.sql.SQLConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uz.ort.api.database.queries.IQuery;
import uz.ort.api.utils.ConfigService;

import java.util.List;

public abstract class AbstructDatabase {

    public static final Logger LOGGER = LoggerFactory.getLogger("AbstructDatabase");

    public JDBCClient dbClient;
    public IQuery query;
    protected Vertx vertx;
    protected JsonObject config;

    public AbstructDatabase(Vertx vertx, JsonObject config) {
        LOGGER.info("Starting AbstructDatabase ..." + this.getClass().getName());
        this.vertx = vertx;
        this.config = config;
        initConnection();
    }

    protected Future<Void> initConnection() {
        Future<Void> future = Future.future();
        this.dbClient = JDBCClient.createShared(
                vertx,
                ConfigService.getPostgreConf(config().getJsonObject("db")),
                config().getJsonObject("db").getString("datasource", "datasourcedbOrt"));
        dbClient.getConnection(ar -> {
            if (ar.failed()) {
                LOGGER.error("Could not open a database connection", ar.cause());
                future.fail(ar.cause());
            } else {
                SQLConnection connection = ar.result();
                LOGGER.info("Connection :" + ar.succeeded());
                future.complete();
            }
        });
        return future;
    }

    private JsonObject config() {
        return config;
    }

    public Future<List<JsonObject>> getList() {
        Future<List<JsonObject>> future = Future.future();
        dbClient.query(
                query.queryList(),
                res -> {
                    if (res.succeeded() && res.result() != null) {
                        future.complete(res.result().getRows());
                    } else {
                        future.fail(res.cause());
                    }
                });
        return future;
    }

}
