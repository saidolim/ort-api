package uz.ort.api.database;

public class QueryKafedra extends AbstractQuery {
    public QueryKafedra() {
        GET_LIST = "" +
                " SELECT id, name, phone, " +
                "        adress, e_mail, " +
                "        date_offoundation " +
                "  FROM public.universitet " +
                "  WHERE status = 'A'; ";
        GET_BY_ID = "" +
                " SELECT id, name, phone, " +
                "        adress, e_mail, " +
                "        date_offoundation " +
                "  FROM public.universitet " +
                "  WHERE status = 'A'" +
                "    AND id = ? ; ";
        INSERT = "" +
                " INSERT INTO public.universitet( " +
                "        id, name, phone, adress, " +
                "        e_mail, date_offoundation) " +
                "    VALUES (?, ?, ?, ?, ?, ?); ";
        UPDATE = "" +
                " UPDATE public.demos " +
                "   SET name = ?, intval = ?, mod_date = now() " +
                " WHERE id = ? " +
                "   AND status = 'A';";
        DELETE = "" +
                " UPDATE public.demos " +
                "   SET status = 'D', exp_date = now() " +
                " WHERE id = ? " +
                "   AND status = 'A';";
    }
}
