package uz.ort.api.database.queries;

public abstract class IQuery {
    public abstract String queryList();

    public abstract String queryListByPage();

    public abstract String queryCountByPage();

    public abstract String queryById();

    public abstract String queryInsert();

    public abstract String queryUpdate();

    public abstract String queryDelete();
}
